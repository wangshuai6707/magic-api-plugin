package org.d8s.magicapi.util;

/**
 * @author wangshuai@e6yun.com
 * @date 6/8/2021 11:20 AM
 **/
public enum ApiStatusCodeEnum {
    /**
     * 接口状态码
     */
    API_STATUS_CODE_1("1", "正确结果"),
    API_STATUS_CODE_200("200", "正确结果"),
    API_STATUS_CODE_5("5", "参数都正确，没有数据"),
    API_STATUS_CODE_11("11", "用户没有访问api的权限"),
    API_STATUS_CODE_21("21", "用户请求未传入method（请求的api名称）参数"),
    API_STATUS_CODE_22("22", "用户请求的method找不到相应的api与之对应"),
    API_STATUS_CODE_24("24", "用户请求未传入sign（请求签名）参数"),
    API_STATUS_CODE_25("25", "用户请求传入的sign参数验证错误。服务端用传入的参数按照约定方法签名得到的值与传入的sign参数不一样"),
    API_STATUS_CODE_28("28", "用户请求未传入appkey（应用标记key）参数"),
    API_STATUS_CODE_29("29", "用户请求传入的appkey找不到对应的应用"),
    API_STATUS_CODE_30("30", "用户请求未传入timestamp（时间戳）参数"),
    API_STATUS_CODE_31("31", "用户请求传入的timestamp参数不是合法的格式"),
    API_STATUS_CODE_32("32", "时间戳不在规定范围内"),
    API_STATUS_CODE_40("40", "少必传参数"),
    API_STATUS_CODE_41("41", "传入的参数格式错误"),
    API_STATUS_CODE_42("42", "详细错误"),
    API_STATUS_CODE_52("52", "服务端执行异常"),
    API_STATUS_CODE_503("503", "超过接口的最大请求频率"),
    API_STATUS_CODE_504("504", "超过接口的最大请求频率");

    private String code;

    private String message;

    ApiStatusCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
