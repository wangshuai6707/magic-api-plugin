package org.d8s.magicapi.util;


/**
 * 常量
 * @author 冰点
 * @date 2021-5-20 14:35:49
 * @since 1.1.1
 */
public class EncryptConstants {

    public enum SignType {
        MD5, HMACSHA256
    }
    public final static String START_PLUGIN_LOG_MSG="已开启[{}]插件成功,如需关闭[{}],插件配置信息:[{}]";
}

